FROM php:7.3-fpm AS php_base

RUN apt-get update && apt-get install -y --no-install-recommends \
    apt-transport-https \
    libxml2-dev \
    git \
    unzip \
    gnupg \
    gzip \
    libzip-dev \
    zlib1g-dev \
    libpng-dev \
    libc-client-dev \
    libpq-dev \
    nginx \
    && rm -r /var/lib/apt/lists/* && rm -f /etc/nginx/sites-enabled/default

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN rm /etc/localtime
RUN ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime
RUN "date"

RUN docker-php-ext-install gd pdo pdo_pgsql soap mbstring iconv json opcache zip \
    && pecl install apcu \
    && docker-php-ext-enable apcu
#    && pecl install sqlsrv pdo_sqlsrv \
#    && docker-php-ext-enable sqlsrv pdo_sqlsrv

#RUN cd /tmp && git clone https://git.php.net/repository/pecl/networking/ssh2.git && cd /tmp/ssh2 \
#    && phpize && ./configure && make && make install \
#    && echo "extension=ssh2" > /usr/local/etc/php/conf.d/ext-ssh2.ini \
#    && rm -rf /tmp/ssh2

COPY docker-conf/www.conf /usr/local/etc/php-fpm.d/
COPY docker-conf/nginx.conf /etc/nginx/
COPY docker-conf/site.conf /etc/nginx/conf.d/
COPY docker-conf/custom_php.ini /usr/local/etc/php/conf.d/
COPY --chown=www-data:www-data . /code/

WORKDIR /code
ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["php-fpm"]

FROM php_base AS php_local

RUN pecl install xdebug-2.7.0RC2 \
    && docker-php-ext-enable xdebug

COPY docker-conf/xdebug.ini /usr/local/etc/php/conf.d/

RUN usermod -u 1000 www-data
RUN groupmod -g 1000 www-data
