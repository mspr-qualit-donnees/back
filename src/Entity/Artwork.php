<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArtworkRepository")
 */
class Artwork
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    private $noticeId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $creationPeriod;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $creationYear;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $length;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $width;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $height;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $importYear;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Museum", inversedBy="artworks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $museum;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Domain", inversedBy="artworks")
     */
    private $domains;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Material", inversedBy="artworks")
     */
    private $materials;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Author", inversedBy="artworks")
     */
    private $authors;

    public function __construct()
    {
        $this->authors = new ArrayCollection();
        $this->domains = new ArrayCollection();
        $this->materials = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNoticeId(): ?string
    {
        return $this->noticeId;
    }

    public function setNoticeId(string $noticeId): self
    {
        $this->noticeId = $noticeId;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCreationPeriod(): ?string
    {
        return $this->creationPeriod;
    }

    public function setCreationPeriod(string $creationPeriod): self
    {
        $this->creationPeriod = $creationPeriod;

        return $this;
    }

    public function getLength(): ?float
    {
        return $this->length;
    }

    public function setLength(float $length): self
    {
        $this->length = $length;

        return $this;
    }

    public function getWidth(): ?float
    {
        return $this->width;
    }

    public function setWidth(float $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getHeight(): ?float
    {
        return $this->height;
    }

    public function setHeight(?float $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getCreationYear(): ?int
    {
        return $this->creationYear;
    }

    public function setCreationYear(?int $creationYear): self
    {
        $this->creationYear = $creationYear;

        return $this;
    }

    public function getImportYear(): ?int
    {
        return $this->importYear;
    }

    public function setImportYear(int $importYear): self
    {
        $this->importYear = $importYear;

        return $this;
    }

    public function getMuseum(): ?Museum
    {
        return $this->museum;
    }

    public function setMuseum(?Museum $museum): self
    {
        $this->museum = $museum;

        return $this;
    }

    public function getDomains(): ?Collection
    {
        return $this->domains;
    }

    public function addDomain(Domain $domain): self
    {
        if (!$this->domains->contains($domain)) {
            $this->domains[] = $domain;
        }

        return $this;
    }

    public function removeDomain(Domain $domain): self
    {
        if ($this->domains->contains($domain)) {
            $this->domains->removeElement($domain);
        }

        return $this;
    }

    public function getMaterials(): ?Collection
    {
        return $this->materials;
    }

    public function addMaterial(Material $material): self
    {
        if (!$this->materials->contains($material)) {
            $this->materials[] = $material;
        }

        return $this;
    }

    public function removeMaterial(Material $material): self
    {
        if ($this->materials->contains($material)) {
            $this->materials->removeElement($material);
        }

        return $this;
    }
    /**
     * @return Collection|Author[]
     */
    public function getAuthors(): Collection
    {
        return $this->authors;
    }

    public function addAuthor(Author $author): self
    {
        if (!$this->authors->contains($author)) {
            $this->authors[] = $author;
        }

        return $this;
    }

    public function removeAuthor(Author $author): self
    {
        if ($this->authors->contains($author)) {
            $this->authors->removeElement($author);
        }

        return $this;
    }
}
