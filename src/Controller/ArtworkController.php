<?php

namespace App\Controller;

use App\Repository\ArtworkRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Rest\Route("/artworks", name="artworks_")
 */
class ArtworkController extends AbstractController
{
    /**
     * @Rest\Get("/count", name="count")
     * @Rest\View(statusCode=200)
     */
    public function getCount(ArtworkRepository $repository): int
    {
        return $repository->count([]);
    }


    /**
     * @Rest\Get("/imports-per-year", name="imports_per_year")
     * @Rest\View(statusCode=200)
     */
    public function getImportsPerYear(ArtworkRepository $repository): array
    {
        return $repository->fillMissingYears($repository->countImportsPerYear());
    }

    /**
     * @Rest\Get("/women-per-year", name="women_per_year")
     * @Rest\View(statusCode=200)
     */
    public function getWomenAuthorsPerYear(ArtworkRepository $repository): array
    {
        return $repository->fillMissingYears($repository->countWomenArtworksPerYear(), $repository->getMinCreationYear(), $repository->getMaxCreationYear(), 25);
    }

    /**
     * @Rest\Get("/total-area", name="total_area")
     * @Rest\View(statusCode=200)
     */
    public function getTotalArea(ArtworkRepository $repository)
    {
        $totalArea = $repository->sumAreas();

        /** Surfaces en cm carrés */
        $comparisons = [
            'football_field' => 71400000,
            'aircraft_carrier' => 168301400,
            'sylvain' => 8500
        ];

        $result = [
            'area' => $totalArea / 10000,
            'in' => [],
            'eq' => [],
            'out' => []
        ];

        if (0 === $totalArea) {
            return $result;
        }

        foreach ($comparisons as $k => $comparison) {
            if ($totalArea < $comparison) {
                $result['out'][$k] = intdiv($comparison, $totalArea);
            } elseif ((float)$totalArea > $comparison) {
                $result['in'][$k] = intdiv($totalArea, $comparison);
            } else {
                $result['eq'][$k] = '1';
            }
        }

        return $result;
    }

    /**
     * @Rest\Get("/quantities-per-domain", name="quantities_per_domain")
     * @Rest\View(statusCode=200)
     */
    public function getQuantitiesPerDomain(ArtworkRepository $repository)
    {
        $domains3D = [
            'peinture' => [
                '3d' => false,
                'unit' => 'L',
                'valueForOne' => 0.0001,
                'productName' => 'Peinture'
            ],
//            'sculpture' => [
//                '3d' => true,
//                'unit' => 'kg',
//                'valueForOne' => 0.000027,
//                'productName' => 'Marbre'
//            ]
        ];

        $results = [];

        foreach ($domains3D as $domain => $definition) {
            if ($definition['3d']) {
                $results[$domain]['volume'] = $total = $repository->sumDomainVolumes($domain);
                $results[$domain]['volume_unit'] = 'cm3';
            } else {
                $results[$domain]['area'] = $total = $repository->sumDomainAreas($domain);
                $results[$domain]['area_unit'] = 'cm2';
            }
            $results[$domain]['quantity_used'] = $total * $definition['valueForOne'];
            $results[$domain]['product_name'] = $definition['productName'];
            $results[$domain]['unit'] = $definition['unit'];
        }

        return $results;
    }
}
