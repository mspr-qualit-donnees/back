<?php

namespace App\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController
{
    /**
     * @Rest\Get("/", name="home")
     * @Rest\View(statusCode=200)
     */
    public function index()
    {
        return [
            'message' => 'DataVinciCode API Works !'
        ];
    }
}
