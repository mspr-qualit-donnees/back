<?php

namespace App\Repository;

use App\Entity\Artwork;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * @method Artwork|null find($id, $lockMode = null, $lockVersion = null)
 * @method Artwork|null findOneBy(array $criteria, array $orderBy = null)
 * @method Artwork[]    findAll()
 * @method Artwork[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArtworkRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Artwork::class);
    }

    public function countImportsPerYear(): array
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('name', 'name')
            ->addScalarResult('value', 'value', 'integer');
        $native = $this->getEntityManager()->createNativeQuery('with data as (SELECT a.import_year as name, COUNT(a.id) as value FROM artwork a WHERE a.import_year IS NOT NULL GROUP BY a.import_year ORDER BY a.import_year ASC) SELECT name, (SUM(value) OVER (ORDER BY name ASC ROWS BETWEEN unbounded preceding AND CURRENT ROW)) as value FROM data d', $rsm);
//        $subQ = $this->createQueryBuilder('a');
//        $subQ->select($subQ->expr()->count('a') . 'as value', 'a.importYear as name')
//            ->groupBy('a.importYear')
//            ->where($subQ->expr()->isNotNull('a.importYear'))
//            ->orderBy('a.importYear', 'ASC');
//
//        $q = $this->createQueryBuilder('artwork');
//        $q->resetDQLParts()->select($q->expr()->sum('value') . ' as value', 'name');

        return array_map(static function ($result) {
            return ['name' => (string)$result['name'], 'value' => (int)$result['value']];
        }, $native->getScalarResult());
    }


    public function countWomenArtworksPerYear(): array
    {
        $q = $this->createQueryBuilder('artwork');
        $q->resetDQLPart('select')->select($q->expr()->count('artwork') . ' as value', 'artwork.creationYear as name')
            ->innerJoin('artwork.authors', 'author')
            ->innerJoin('author.gender', 'gender')
            ->where($q->expr()->eq('gender.label', $q->expr()->literal('F')))
            ->groupBy('artwork.creationYear')
            ->orderBy('artwork.creationYear', 'ASC')
        ;

        return $q->getQuery()->getScalarResult();
    }

    public function getMinImportYear(): int
    {
        $q = $this->createQueryBuilder('artwork');
        $q->resetDQLPart('select')->select($q->expr()->min('artwork.importYear'));
        return (int)$q->getQuery()->getSingleScalarResult();
    }

    public function getMaxImportYear(): int
    {
        $q = $this->createQueryBuilder('artwork');
        $q->resetDQLPart('select')->select($q->expr()->max('artwork.importYear'));
        return (int)$q->getQuery()->getSingleScalarResult();
    }

    public function getMinCreationYear(): int
    {
        $q = $this->createQueryBuilder('artwork');
        $q->resetDQLPart('select')->select($q->expr()->min('artwork.creationYear'));
        return (int)$q->getQuery()->getSingleScalarResult();
    }

    public function getMaxCreationYear(): int
    {
        $q = $this->createQueryBuilder('artwork');
        $q->resetDQLPart('select')->select($q->expr()->max('artwork.creationYear'));
        return (int)$q->getQuery()->getSingleScalarResult();
    }

    public function fillMissingYears(array $yearCountResult = [], $min = null, $max = null, $step = 1): array
    {
        if (empty($yearCountResult)) {
            return [];
        }

        $minYear = $min ?? reset($yearCountResult)['name'];
        $maxYear = $max ?? end($yearCountResult)['name'];

        $realCounts = range($minYear, $maxYear, $step);

        $realCounts = array_combine($realCounts, array_map(static function ($year) {
            return ['value' => 0, 'name' => (string)$year];
        }, $realCounts));

        $lastDbRes = reset($yearCountResult)['value'];

        array_walk($realCounts, static function (&$count) use (&$yearCountResult, &$lastDbRes) {
            $unset = false;
            $k = 0;
            foreach ($yearCountResult as $k => $dbItem) {
                if ($dbItem['name'] === $count['name']) {
                    $lastDbRes = $dbItem['value'];
                    $unset = true;
                    break;
                }
            }
            if ($unset) {
                unset($yearCountResult[$k]);
            }
            if (0 === $count['value']) {
                $count['value'] = $lastDbRes;
            }
        });

        return array_values($realCounts);
    }

    public function sumAreas(): int
    {
        $q = $this->createQueryBuilder('artwork');

        $q->resetDQLPart('select')->select('SUM(artwork.length*artwork.width) as area')
            ->where($q->expr()->andX(
                $q->expr()->isNotNull('artwork.length'),
                $q->expr()->isNotNull('artwork.width')
            ));

        return (int)$q->getQuery()->getSingleScalarResult();
    }

    public function sumDomainVolumes(string $domain): int
    {
        $q = $this->createQueryBuilder('artwork');

        $q->resetDQLPart('select')->select('SUM(artwork.length*artwork.width*artwork.height) as volume')
            ->innerJoin('artwork.domains', 'domain')
            ->where($q->expr()->andX(
                $q->expr()->eq('domain.label', $q->expr()->literal($domain)),
                $q->expr()->isNotNull('artwork.length'),
                $q->expr()->isNotNull('artwork.height'),
                $q->expr()->isNotNull('artwork.width')
            ));

        return (int)$q->getQuery()->getSingleScalarResult();
    }

    public function sumDomainAreas(string $domain): int
    {
        $q = $this->createQueryBuilder('artwork');

        $q->resetDQLPart('select')->select('SUM(artwork.length*artwork.width) as area')
            ->innerJoin('artwork.domains', 'domain')
            ->where($q->expr()->andX(
                $q->expr()->eq('domain.label', $q->expr()->literal($domain)),
                $q->expr()->isNotNull('artwork.length'),
                $q->expr()->isNotNull('artwork.width')
            ));

        return (int)$q->getQuery()->getSingleScalarResult();
    }

    // /**
    //  * @return Artwork[] Returns an array of Artwork objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Artwork
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
