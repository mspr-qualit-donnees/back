<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191030143650 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE artwork_domain (artwork_id INT NOT NULL, domain_id INT NOT NULL, PRIMARY KEY(artwork_id, domain_id))');
        $this->addSql('CREATE INDEX IDX_5A5A0D19DB8FFA4 ON artwork_domain (artwork_id)');
        $this->addSql('CREATE INDEX IDX_5A5A0D19115F0EE5 ON artwork_domain (domain_id)');
        $this->addSql('CREATE TABLE artwork_material (artwork_id INT NOT NULL, material_id INT NOT NULL, PRIMARY KEY(artwork_id, material_id))');
        $this->addSql('CREATE INDEX IDX_80F4B96BDB8FFA4 ON artwork_material (artwork_id)');
        $this->addSql('CREATE INDEX IDX_80F4B96BE308AC6F ON artwork_material (material_id)');
        $this->addSql('ALTER TABLE artwork_domain ADD CONSTRAINT FK_5A5A0D19DB8FFA4 FOREIGN KEY (artwork_id) REFERENCES artwork (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE artwork_domain ADD CONSTRAINT FK_5A5A0D19115F0EE5 FOREIGN KEY (domain_id) REFERENCES domain (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE artwork_material ADD CONSTRAINT FK_80F4B96BDB8FFA4 FOREIGN KEY (artwork_id) REFERENCES artwork (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE artwork_material ADD CONSTRAINT FK_80F4B96BE308AC6F FOREIGN KEY (material_id) REFERENCES material (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE artwork DROP CONSTRAINT fk_881fc576115f0ee5');
        $this->addSql('ALTER TABLE artwork DROP CONSTRAINT fk_881fc576e308ac6f');
        $this->addSql('DROP INDEX idx_881fc576e308ac6f');
        $this->addSql('DROP INDEX idx_881fc576115f0ee5');
        $this->addSql('ALTER TABLE artwork DROP domain_id');
        $this->addSql('ALTER TABLE artwork DROP material_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE artwork_domain');
        $this->addSql('DROP TABLE artwork_material');
        $this->addSql('ALTER TABLE artwork ADD domain_id INT NOT NULL');
        $this->addSql('ALTER TABLE artwork ADD material_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE artwork ADD CONSTRAINT fk_881fc576115f0ee5 FOREIGN KEY (domain_id) REFERENCES domain (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE artwork ADD CONSTRAINT fk_881fc576e308ac6f FOREIGN KEY (material_id) REFERENCES material (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_881fc576e308ac6f ON artwork (material_id)');
        $this->addSql('CREATE INDEX idx_881fc576115f0ee5 ON artwork (domain_id)');
    }
}
