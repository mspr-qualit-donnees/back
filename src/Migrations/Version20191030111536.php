<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191030111536 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE material_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE artwork_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE gender_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE location_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE domain_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE author_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE museum_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE material (id INT NOT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE artwork (id INT NOT NULL, museum_id INT NOT NULL, domain_id INT NOT NULL, material_id INT DEFAULT NULL, notice_id VARCHAR(50) NOT NULL, title VARCHAR(255) NOT NULL, creation_period VARCHAR(255) NOT NULL, creation_year INT NULL, length DOUBLE PRECISION NOT NULL, width DOUBLE PRECISION NOT NULL, height DOUBLE PRECISION DEFAULT NULL, import_year INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_881FC5767D540AB ON artwork (notice_id)');
        $this->addSql('CREATE INDEX IDX_881FC5764B52E5B5 ON artwork (museum_id)');
        $this->addSql('CREATE INDEX IDX_881FC576115F0EE5 ON artwork (domain_id)');
        $this->addSql('CREATE INDEX IDX_881FC576E308AC6F ON artwork (material_id)');
        $this->addSql('CREATE TABLE artwork_author (artwork_id INT NOT NULL, author_id INT NOT NULL, PRIMARY KEY(artwork_id, author_id))');
        $this->addSql('CREATE INDEX IDX_405CCBDADB8FFA4 ON artwork_author (artwork_id)');
        $this->addSql('CREATE INDEX IDX_405CCBDAF675F31B ON artwork_author (author_id)');
        $this->addSql('CREATE TABLE gender (id INT NOT NULL, label VARCHAR(1) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE location (id INT NOT NULL, city VARCHAR(255) NOT NULL, zip_code VARCHAR(20) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE domain (id INT NOT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE author (id INT NOT NULL, gender_id INT NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, birth_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, death_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_BDAFD8C8708A0E0 ON author (gender_id)');
        $this->addSql('CREATE TABLE museum (id INT NOT NULL, location_id INT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6247447777153098 ON museum (code)');
        $this->addSql('CREATE INDEX IDX_6247447764D218E ON museum (location_id)');
        $this->addSql('ALTER TABLE artwork ADD CONSTRAINT FK_881FC5764B52E5B5 FOREIGN KEY (museum_id) REFERENCES museum (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE artwork ADD CONSTRAINT FK_881FC576115F0EE5 FOREIGN KEY (domain_id) REFERENCES domain (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE artwork ADD CONSTRAINT FK_881FC576E308AC6F FOREIGN KEY (material_id) REFERENCES material (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE artwork_author ADD CONSTRAINT FK_405CCBDADB8FFA4 FOREIGN KEY (artwork_id) REFERENCES artwork (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE artwork_author ADD CONSTRAINT FK_405CCBDAF675F31B FOREIGN KEY (author_id) REFERENCES author (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE author ADD CONSTRAINT FK_BDAFD8C8708A0E0 FOREIGN KEY (gender_id) REFERENCES gender (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE museum ADD CONSTRAINT FK_6247447764D218E FOREIGN KEY (location_id) REFERENCES location (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE artwork DROP CONSTRAINT FK_881FC576C54C8C93');
        $this->addSql('ALTER TABLE artwork DROP CONSTRAINT FK_881FC576E308AC6F');
        $this->addSql('ALTER TABLE artwork_author DROP CONSTRAINT FK_405CCBDADB8FFA4');
        $this->addSql('ALTER TABLE author DROP CONSTRAINT FK_BDAFD8C8708A0E0');
        $this->addSql('ALTER TABLE museum DROP CONSTRAINT FK_6247447764D218E');
        $this->addSql('ALTER TABLE artwork DROP CONSTRAINT FK_881FC576115F0EE5');
        $this->addSql('ALTER TABLE artwork_author DROP CONSTRAINT FK_405CCBDAF675F31B');
        $this->addSql('ALTER TABLE artwork DROP CONSTRAINT FK_881FC5764B52E5B5');
        $this->addSql('DROP SEQUENCE material_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE artwork_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE gender_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE location_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE domain_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE author_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE museum_id_seq CASCADE');
        $this->addSql('DROP TABLE material');
        $this->addSql('DROP TABLE artwork');
        $this->addSql('DROP TABLE artwork_author');
        $this->addSql('DROP TABLE gender');
        $this->addSql('DROP TABLE location');
        $this->addSql('DROP TABLE domain');
        $this->addSql('DROP TABLE author');
        $this->addSql('DROP TABLE museum');
    }
}
