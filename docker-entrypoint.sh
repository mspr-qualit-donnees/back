#!/bin/sh
set -e

service nginx start

# Install dependencies, and execute composer tasks
composer install
#yarn
#yarn run encore production
php bin/console cache:clear
chown www-data:www-data -R /code/var

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

exec "$@"